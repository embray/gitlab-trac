# [START dockerfile]
FROM gcr.io/google_appengine/python

# Build requirements for libgit2
RUN apt-get update && \
    apt-get install -y cmake git && \
    apt-get clean

# Build and install libgit2
WORKDIR /src
RUN wget https://github.com/libgit2/libgit2/archive/v0.27.0.tar.gz && \
    tar xzf v0.27.0.tar.gz && \
    cd libgit2-0.27.0/ && \
    cmake . && \
    make && \
    make install && \
    rm -rf v0.27.0* && \
    ldconfig

WORKDIR /app

# Change the -p argument to use Python 2.7 if desired.
RUN virtualenv /env -p python3.6

# Set virtualenv environment variables. This is equivalent to running
# source /env/bin/activate.
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

ADD requirements.txt /app/
RUN pip install -r requirements.txt
ADD . /app/

ARG REPOSITORY_URL=git://git.sagemath.org/sage.git
ENV REPOSITORY_URL=${REPOSITORY_URL}
ARG REPOSITORY_BRANCH=develop
ENV REPOSITORY_BRANCH=${REPOSITORY_BRANCH}
VOLUME /app/repo.git

ENTRYPOINT [ "/app/entrypoint.sh" ]

CMD gunicorn -b :$PORT main:app
# [END dockerfile]
