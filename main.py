# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]
import logging
import subprocess

from flask import Flask, request


app = Flask(__name__)

@app.before_first_request
def setup_logging():
    app.logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(
        '[%(asctime)s] [%(levelname)s] [%(module)s]: %(message)s'
    ))
    app.logger.addHandler(handler)


# [START example]
@app.route('/', methods=['POST'])
def main():
    if ('X-Gitlab-Event' in request.headers and
            request.headers['X-Gitlab-Event'] == 'Merge Request Hook'):
        do_merge_request_hook()
    return ('', 200, {'Content-Type': 'text/plain; charset=utf-8'})
# [END example]


def do_merge_request_hook():
    from pprint import pformat
    app.logger.info(pformat(request.get_json()))


@app.errorhandler(500)
def server_error(e):
    app.logger.exception('An error occurred during a request: {}'.format(e))
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See CMD in Dockerfile.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END app]
