#!/bin/sh
set -e

if [ ! -d "repo.git" ]; then
    echo "Initial clone of git repository $REPOSITORY_URL ..."
    git clone --bare --single-branch -b "$REPOSITORY_BRANCH" \
        "$REPOSITORY_URL" repo.git
fi

exec "$@"
